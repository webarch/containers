# Docker Containers

The Docker containers which are automatically built from this repo, using GitLab CI, are designed for use by GitLab CI on [git.coop](https://git.coop/).

## 2025-03-04 images

2025-03-04 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.44.0](https://git.coop/webarch/ansible/-/releases/3.44.0) which provides `ansible` 10.7.0, `ansible-core` 2.17.9 and [molecule 25.3.1](https://github.com/ansible/molecule/releases/tag/v25.3.1) which requires the [`ANSIBLE_ROLES_PATH` env var to be set](https://github.com/ansible/molecule/issues/4391#issuecomment-2662629041) for GitLab CI, for example:

```yaml
  ANSIBLE_ROLES_PATH: "${CI_PROJECT_DIR}/..:~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles"
```

* `trixie:20250304`
* `bookworm:20250304`
* `noble:20250304`
* `jammy:20250304`
* `trixie:20250304`

## 2025-02-13 images

2025-02-13 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.41.0](https://git.coop/webarch/ansible/-/releases/3.41.0) which provides `ansible` 10.7.0, `ansible-core` 2.17.8 and [molecule 25.2.0](https://github.com/ansible/molecule/releases/tag/v25.2.0) which requires the `ANSIBLE_ROLES_PATH` env var to be set for GitLab CI, for example:

```yaml
  ANSIBLE_ROLES_PATH: "${CI_PROJECT_DIR}/..:~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles"
```

* `trixie:20250213`
* `bookworm:20250213`
* `noble:20250213`
* `jammy:20250213`
* `trixie:20250213`

## 2025-01-13 images

2025-01-13 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.36.0](https://git.coop/webarch/ansible/-/releases/3.36.0) which provides `ansible` 10.7.0 and `ansible-core` 2.17.7.

* `trixie:20250113`
* `bookworm:20250113`
* `noble:20250113`
* `jammy:20250113`
* `trixie:20250113`

## 2024-11-07 images

2024-11-07 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.28.0](https://git.coop/webarch/ansible/-/releases/3.28.0) which provides `ansible` 10.6.0 and `ansible-core` 2.17.6.

* `trixie:20241107`
* `bookworm:20241107`
* `noble:20241107`
* `jammy:20241107`
* `trixie:20241107`

## 2024-10-08 images

2024-10-08 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.27.0](https://git.coop/webarch/ansible/-/releases/3.27.0) which provides `ansible` 10.5.0 and `ansible-core` 2.17.5.

* `trixie:20241008`
* `bookworm:20241008`
* `noble:20241008`
* `jammy:20241008`
* `trixie:20241008`

## 2024-09-11 images

2024-09-11 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.25.1](https://git.coop/webarch/ansible/-/releases/3.25.1) which provides `ansible` 10.4.0 and `ansible-core` 2.17.4.

* `trixie:20240911`
* `bookworm:20240911`
* `noble:20240911`
* `jammy:20240911`
* `trixie:20240911`

## 2024-08-14 images

2024-08-14 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.21.0](https://git.coop/webarch/ansible/-/releases/3.21.0) which provides `ansible` 10.3.0 and `ansible-core` 2.17.3.

* `trixie:20240814`
* `bookworm:20240814`
* `noble:20240814`
* `jammy:20240814`
* `trixie:20240814`

## 2024-07-21 images

2024-07-21 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.19.0](https://git.coop/webarch/ansible/-/releases/3.19.0) which provides `ansible` 10.2.0 and `ansible-core` 2.17.2.

* `trixie:20240721`
* `bookworm:20240721`
* `noble:20240721`
* `jammy:20240721`
* `trixie:20240721`

## 2024-06-05 images

2024-06-05 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.16.0](https://git.coop/webarch/ansible/-/releases/3.16.0) which provides `ansible` 10.0.0 and `ansible-core` 2.17.0.

* `trixie:20240605`
* `bookworm:20240605`
* `noble:20240605`
* `jammy:20240605`
* `trixie:20240605`

## 2024-05-21 images

2024-05-21 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.15.0](https://git.coop/webarch/ansible/-/releases/3.15.0) which provides `ansible` 9.6.0 and `ansible-core` 2.17.0.

* `trixie:20240521`
* `bookworm:20240521`
* `noble:20240521`
* `jammy:20240521`
* `trixie:20240521`

## 2024-04-26 images

2024-04-26 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5) and `ansible` installed using the [Ansible role version 3.13.0](https://git.coop/webarch/ansible/-/releases/3.13.0) which provides `ansible` 9.5.1 and `ansible-core` 2.16.6.

* `trixie:20240426`
* `bookworm:20240426`
* `noble:20240426`
* `jammy:20240426`
* `trixie:20240426`

## 2024-03-26 images

2024-03-26 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20240326

Debian Trixie with Ansible installed with [version 3.10.0](https://git.coop/webarch/ansible/-/releases/3.10.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.4.0](https://github.com/ansible-community/ansible-build-data/blob/9.4.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.5](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-5).

### bookworm:20240326

Debian Bookworm with Ansible installed with [version 3.10.0](https://git.coop/webarch/ansible/-/releases/3.10.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.4.0](https://github.com/ansible-community/ansible-build-data/blob/9.4.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.5](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-5).

#### bullseye:20240326

Debian Bookworm with Ansible installed with [version 3.10.0](https://git.coop/webarch/ansible/-/releases/3.10.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 8.7.0](https://raw.githubusercontent.com/ansible-community/ansible-build-data/main/8/CHANGELOG-v8.rst) and [ansible-core 2.15.10](https://github.com/ansible/ansible/blob/stable-2.15/changelogs/CHANGELOG-v2.15.rst#v2-15-10).

### noble:20240326

Ubuntu Noble with Ansible installed with [version 3.10.0](https://git.coop/webarch/ansible/-/releases/3.10.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.4.0](https://github.com/ansible-community/ansible-build-data/blob/9.4.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.5](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-5).

### jammy:20240326

Ubuntu Jammy with Ansible installed with [version 3.10.0](https://git.coop/webarch/ansible/-/releases/3.10.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.4.0](https://github.com/ansible-community/ansible-build-data/blob/9.4.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.5](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-5).

## 2024-02-09 images

2024-02-09 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20240209

Debian Trixie with Ansible installed with [version 3.9.1](https://git.coop/webarch/ansible/-/releases/3.9.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.2.0](https://github.com/ansible-community/ansible-build-data/blob/9.2.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.3](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-3).

### bookworm:20240209

Debian Trixie with Ansible installed with [version 3.9.1](https://git.coop/webarch/ansible/-/releases/3.9.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.2.0](https://github.com/ansible-community/ansible-build-data/blob/9.2.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.3](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-3).

#### bullseye:20240209

Debian Trixie with Ansible installed with [version 3.9.1](https://git.coop/webarch/ansible/-/releases/3.9.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 8.7.0](https://raw.githubusercontent.com/ansible-community/ansible-build-data/main/8/CHANGELOG-v8.rst) and [ansible-core 2.15.8](https://github.com/ansible/ansible/blob/stable-2.15/changelogs/CHANGELOG-v2.15.rst#v2-15-8)

### jammy:20240209

Debian Trixie with Ansible installed with [version 3.9.1](https://git.coop/webarch/ansible/-/releases/3.9.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.2.0](https://github.com/ansible-community/ansible-build-data/blob/9.2.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.3](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-3).

## 2024-01-05 images

2024-01-05 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20240105

Debian Trixie with Ansible installed with [version 3.8.2](https://git.coop/webarch/ansible/-/releases/3.8.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

### bookworm:20240105

Debian Trixie with Ansible installed with [version 3.8.2](https://git.coop/webarch/ansible/-/releases/3.8.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

#### bullseye:20240105

Debian Trixie with Ansible installed with [version 3.8.2](https://git.coop/webarch/ansible/-/releases/3.8.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 8.7.0](https://raw.githubusercontent.com/ansible-community/ansible-build-data/main/8/CHANGELOG-v8.rst) and [ansible-core 2.15.8](https://github.com/ansible/ansible/blob/stable-2.15/changelogs/CHANGELOG-v2.15.rst#v2-15-8)

### jammy:20240105

Debian Trixie with Ansible installed with [version 3.8.2](https://git.coop/webarch/ansible/-/releases/3.8.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

## 2023-12-13 images

2023-12-13 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20231213

Debian Trixie with Ansible installed with [version 3.6.2](https://git.coop/webarch/ansible/-/releases/3.6.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

### bookworm:20231213

Debian Trixie with Ansible installed with [version 3.6.2](https://git.coop/webarch/ansible/-/releases/3.6.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

#### bullseye:20231213

Debian Trixie with Ansible installed with [version 3.6.2](https://git.coop/webarch/ansible/-/releases/3.6.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 8.7.0](https://raw.githubusercontent.com/ansible-community/ansible-build-data/main/8/CHANGELOG-v8.rst) and [ansible-core 2.15.8](https://github.com/ansible/ansible/blob/stable-2.15/changelogs/CHANGELOG-v2.15.rst#v2-15-8)

### jammy:20231213

Debian Trixie with Ansible installed with [version 3.6.2](https://git.coop/webarch/ansible/-/releases/3.6.2) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst) and [ansible-core 2.16.2](https://github.com/ansible/ansible/blob/stable-2.16/changelogs/CHANGELOG-v2.16.rst#v2-16-2).

## 2023-12-06 images

2023-12-06 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20231206

Debian Trixie with Ansible installed with [version 3.4.0](https://git.coop/webarch/ansible/-/releases/3.4.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst).

### bookworm:20231206

Debian Trixie with Ansible installed with [version 3.4.0](https://git.coop/webarch/ansible/-/releases/3.4.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst).

#### bullseye:20231206

Debian Trixie with Ansible installed with [version 3.4.0](https://git.coop/webarch/ansible/-/releases/3.4.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst).

### jammy:20231206

Debian Trixie with Ansible installed with [version 3.4.0](https://git.coop/webarch/ansible/-/releases/3.4.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides [Ansible 9.1.0](https://github.com/ansible-community/ansible-build-data/blob/9.1.0/9/CHANGELOG-v9.rst).

## 2023-11-19 images

2023-11-19 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20231119

Debian Trixie with Ansible installed with [version 3.2.0](https://git.coop/webarch/ansible/-/releases/3.2.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 7.0.0
  ansible.netcommon:
    version: 5.3.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 2.1.0
  arista.eos:
    version: 6.2.1
  awx.awx:
    version: 23.3.1
  azure.azcollection:
    version: 1.19.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.8.0
  cisco.asa:
    version: 4.0.3
  cisco.dnac:
    version: 6.7.6
  cisco.intersight:
    version: 2.0.3
  cisco.ios:
    version: 5.2.0
  cisco.iosxr:
    version: 6.1.0
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.14
  cisco.mso:
    version: 2.5.0
  cisco.nxos:
    version: 5.2.1
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 7.0.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.7
  community.crypto:
    version: 2.16.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.3
  community.docker:
    version: 3.4.11
  community.general:
    version: 8.0.2
  community.grafana:
    version: 1.6.1
  community.hashi_vault:
    version: 6.0.0
  community.hrobot:
    version: 1.8.2
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.8.0
  community.network:
    version: 5.0.2
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 3.2.0
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 2.0.0
  community.sap_libs:
    version: 1.4.1
  community.sops:
    version: 1.6.7
  community.vmware:
    version: 4.0.0
  community.windows:
    version: 2.0.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.11.0
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 8.4.0
  dellemc.powerflex:
    version: 2.0.1
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.27.0
  fortinet.fortimanager:
    version: 2.3.0
  fortinet.fortios:
    version: 2.3.4
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 2.3.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 2.0.0
  ibm.storage_virtualize:
    version: 2.1.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 2.1.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.2
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.1
  netapp.azure:
    version: 21.10.1
  netapp.cloudmanager:
    version: 21.22.1
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.8.2
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.1
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.15.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.22.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 2.0.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.22.0 using ansible-core:2.16.0 ansible-compat:4.1.10 ruamel-yaml:0.18.5 ruamel-yaml-clib:0.2.8
ansible [core 2.16.0]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.6 (main, Oct  8 2023, 05:06:43) [GCC 13.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.33.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.16.0
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### bookworm:20231119

Debian Bookworm with Ansible installed with [version 3.2.0](https://git.coop/webarch/ansible/-/releases/3.2.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 7.0.0
  ansible.netcommon:
    version: 5.3.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 2.1.0
  arista.eos:
    version: 6.2.1
  awx.awx:
    version: 23.3.1
  azure.azcollection:
    version: 1.19.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.8.0
  cisco.asa:
    version: 4.0.3
  cisco.dnac:
    version: 6.7.6
  cisco.intersight:
    version: 2.0.3
  cisco.ios:
    version: 5.2.0
  cisco.iosxr:
    version: 6.1.0
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.14
  cisco.mso:
    version: 2.5.0
  cisco.nxos:
    version: 5.2.1
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 7.0.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.7
  community.crypto:
    version: 2.16.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.3
  community.docker:
    version: 3.4.11
  community.general:
    version: 8.0.2
  community.grafana:
    version: 1.6.1
  community.hashi_vault:
    version: 6.0.0
  community.hrobot:
    version: 1.8.2
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.8.0
  community.network:
    version: 5.0.2
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 3.2.0
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 2.0.0
  community.sap_libs:
    version: 1.4.1
  community.sops:
    version: 1.6.7
  community.vmware:
    version: 4.0.0
  community.windows:
    version: 2.0.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.11.0
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 8.4.0
  dellemc.powerflex:
    version: 2.0.1
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.27.0
  fortinet.fortimanager:
    version: 2.3.0
  fortinet.fortios:
    version: 2.3.4
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 2.3.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 2.0.0
  ibm.storage_virtualize:
    version: 2.1.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 2.1.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.2
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.1
  netapp.azure:
    version: 21.10.1
  netapp.cloudmanager:
    version: 21.22.1
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.8.2
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.1
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.15.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.22.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 2.0.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.22.0 using ansible-core:2.16.0 ansible-compat:4.1.10 ruamel-yaml:0.18.5 ruamel-yaml-clib:0.2.8
ansible [core 2.16.0]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.33.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.16.0
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### bullseye:20231119

Debian Bullseye with Ansible installed with [version 3.2.0](https://git.coop/webarch/ansible/-/releases/3.2.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.9/site-packages/ansible_collections:
  amazon.aws:
    version: 6.5.0
  ansible.netcommon:
    version: 5.3.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.2.1
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.19.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.8.0
  cisco.asa:
    version: 4.0.3
  cisco.dnac:
    version: 6.7.6
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.13
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.4.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.7
  community.crypto:
    version: 2.16.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.3
  community.docker:
    version: 3.4.10
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.5.1
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.6.1
  community.hashi_vault:
    version: 5.0.1
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.8.0
  community.network:
    version: 5.0.2
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.7
  community.vmware:
    version: 3.11.1
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.11.0
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.9.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.27.0
  fortinet.fortimanager:
    version: 2.3.0
  fortinet.fortios:
    version: 2.3.4
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  ibm.storage_virtualize:
    version: 2.1.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.2
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.1
  netapp.azure:
    version: 21.10.1
  netapp.cloudmanager:
    version: 21.22.1
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.8.2
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.1
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.15.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.22.0 using ansible-core:2.15.6 ansible-compat:4.1.10 ruamel-yaml:None ruamel-yaml-clib:None
/usr/local/bin/ansible
ansible [core 2.15.6]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.33.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.9
    ansible:2.15.6
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### jammy:20231119

Debian Jammy with Ansible installed with [version 3.2.0](https://git.coop/webarch/ansible/-/releases/3.2.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.10/site-packages/ansible_collections:
  amazon.aws:
    version: 7.0.0
  ansible.netcommon:
    version: 5.3.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 2.1.0
  arista.eos:
    version: 6.2.1
  awx.awx:
    version: 23.3.1
  azure.azcollection:
    version: 1.19.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.8.0
  cisco.asa:
    version: 4.0.3
  cisco.dnac:
    version: 6.7.6
  cisco.intersight:
    version: 2.0.3
  cisco.ios:
    version: 5.2.0
  cisco.iosxr:
    version: 6.1.0
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.14
  cisco.mso:
    version: 2.5.0
  cisco.nxos:
    version: 5.2.1
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 7.0.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.7
  community.crypto:
    version: 2.16.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.3
  community.docker:
    version: 3.4.11
  community.general:
    version: 8.0.2
  community.grafana:
    version: 1.6.1
  community.hashi_vault:
    version: 6.0.0
  community.hrobot:
    version: 1.8.2
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.8.0
  community.network:
    version: 5.0.2
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 3.2.0
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 2.0.0
  community.sap_libs:
    version: 1.4.1
  community.sops:
    version: 1.6.7
  community.vmware:
    version: 4.0.0
  community.windows:
    version: 2.0.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.11.0
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 8.4.0
  dellemc.powerflex:
    version: 2.0.1
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.27.0
  fortinet.fortimanager:
    version: 2.3.0
  fortinet.fortios:
    version: 2.3.4
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 2.3.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 2.0.0
  ibm.storage_virtualize:
    version: 2.1.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 2.1.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.2
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.1
  netapp.azure:
    version: 21.10.1
  netapp.cloudmanager:
    version: 21.22.1
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.8.2
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.1
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.15.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.22.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 2.0.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.22.0 using ansible-core:2.16.0 ansible-compat:4.1.10 ruamel-yaml:0.18.5 ruamel-yaml-clib:0.2.8
/usr/local/bin/ansible
ansible [core 2.16.0]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.33.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.10
    ansible:2.16.0
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

## 2023-10-16 images

2023-10-16 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20231016

Debian Trixie with Ansible installed with [version 3.0.1](https://git.coop/webarch/ansible/-/releases/3.0.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.5.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.2
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.18.1
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.2
  cisco.dnac:
    version: 6.7.5
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.5
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.2
  community.docker:
    version: 3.4.9
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.5.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.6
  community.vmware:
    version: 3.10.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.9.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  community.postgresql:
    version: 3.2.0
/usr/local/bin/ansible-lint
ansible-lint 6.20.3 using ansible-core:2.15.5 ansible-compat:4.1.10 ruamel-yaml:0.17.35 ruamel-yaml-clib:0.2.8
ansible [core 2.15.5]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.6 (main, Oct  8 2023, 05:06:43) [GCC 13.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.15.5
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### bookworm:20231016

Debian Bookworm with Ansible installed with [version 3.0.1](https://git.coop/webarch/ansible/-/releases/3.0.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.5.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.2
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.18.1
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.2
  cisco.dnac:
    version: 6.7.5
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.5
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.2
  community.docker:
    version: 3.4.9
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.5.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.6
  community.vmware:
    version: 3.10.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.9.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  community.postgresql:
    version: 3.2.0
/usr/local/bin/ansible-lint
ansible-lint 6.20.3 using ansible-core:2.15.5 ansible-compat:4.1.10 ruamel-yaml:0.17.35 ruamel-yaml-clib:0.2.8
ansible [core 2.15.5]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.15.5
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### bullseye:20231016

Debian Bullseye with Ansible installed with [version 3.0.1](https://git.coop/webarch/ansible/-/releases/3.0.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.9/site-packages/ansible_collections:
  amazon.aws:
    version: 6.5.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.2
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.18.1
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.2
  cisco.dnac:
    version: 6.7.5
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.5
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.2
  community.docker:
    version: 3.4.9
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.5.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.6
  community.vmware:
    version: 3.10.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.9.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  community.postgresql:
    version: 3.2.0
/usr/local/bin/ansible-lint
ansible-lint 6.20.3 using ansible-core:2.15.5 ansible-compat:4.1.10 ruamel-yaml:None ruamel-yaml-clib:None
/usr/local/bin/ansible
ansible [core 2.15.5]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.9
    ansible:2.15.5
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

### jammy:20231016

Ubuntu Jammy with Ansible installed with [version 3.0.1](https://git.coop/webarch/ansible/-/releases/3.0.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

<details>
<summary>Ansible details</summary>
<pre>
/opt/venvs/ansible/lib/python3.10/site-packages/ansible_collections:
  amazon.aws:
    version: 6.5.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.2
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.18.1
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.2
  cisco.dnac:
    version: 6.7.5
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.16
  cisco.meraki:
    version: 2.16.5
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.2
  community.docker:
    version: 3.4.9
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.5.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.3.0
  community.mongodb:
    version: 1.6.3
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.10.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.6
  community.vmware:
    version: 3.10.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.2
  cyberark.pas:
    version: 1.0.23
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.9.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.2.3
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.2.0
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.14.0
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.10.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  community.postgresql:
    version: 3.2.0
/usr/local/bin/ansible-lint
ansible-lint 6.20.3 using ansible-core:2.15.5 ansible-compat:4.1.10 ruamel-yaml:0.17.35 ruamel-yaml-clib:0.2.8
/usr/local/bin/ansible
ansible [core 2.15.5]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.10
    ansible:2.15.5
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
</pre>
</details>

## 2023-09-21 images

2023-09-21 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20230921

Debian Trixie with Ansible installed with [version 2.20.0](https://git.coop/webarch/ansible/-/releases/2.20.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.4.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.0
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.17.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.4
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.15
  cisco.meraki:
    version: 2.16.0
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.1
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.4.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.5
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.8.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.8
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.3
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.13.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.9.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  ansible.netcommon:
    version: 5.2.0
  ansible.utils:
    version: 2.11.0
  community.crypto:
    version: 2.15.1
  community.general:
    version: 7.4.0
  community.postgresql:
    version: 3.2.0
  hetzner.hcloud:
    version: 2.1.1
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.20.0 using ansible-core:2.15.4 ansible-compat:4.1.10 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
ansible [core 2.15.4]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.5 (main, Aug 29 2023, 15:31:31) [GCC 13.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.15.4
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bookworm:20230921

Debian Bookworm with Ansible installed with [version 2.20.0](https://git.coop/webarch/ansible/-/releases/2.20.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.4.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.0
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.17.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.4
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.15
  cisco.meraki:
    version: 2.16.0
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.1
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.4.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.5
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.8.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.8
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.3
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.13.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.9.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  ansible.netcommon:
    version: 5.2.0
  ansible.utils:
    version: 2.11.0
  community.crypto:
    version: 2.15.1
  community.general:
    version: 7.4.0
  community.mysql:
    version: 3.7.2
  community.postgresql:
    version: 3.2.0
  hetzner.hcloud:
    version: 2.1.1
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.20.0 using ansible-core:2.15.4 ansible-compat:4.1.10 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
ansible [core 2.15.4]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.11
    ansible:2.15.4
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bullseye:20230921

Debian Bullseye with Ansible installed with [version 2.20.0](https://git.coop/webarch/ansible/-/releases/2.20.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.9/site-packages/ansible_collections:
  amazon.aws:
    version: 6.4.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.0
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.17.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.4
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.15
  cisco.meraki:
    version: 2.16.0
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.1
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.4.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.5
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.8.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.8
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.3
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.13.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.9.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  ansible.netcommon:
    version: 5.2.0
  ansible.utils:
    version: 2.11.0
  community.crypto:
    version: 2.15.1
  community.general:
    version: 7.4.0
  community.mysql:
    version: 3.7.2
  community.postgresql:
    version: 3.2.0
  hetzner.hcloud:
    version: 2.1.1
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.20.0 using ansible-core:2.15.4 ansible-compat:4.1.10 ruamel-yaml:None ruamel-yaml-clib:None
/usr/local/bin/ansible
ansible [core 2.15.4]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.9
    ansible:2.15.4
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### jammy:20230921

Ubuntu Jammy with Ansible installed with [version 2.20.0](https://git.coop/webarch/ansible/-/releases/2.20.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.10/site-packages/ansible_collections:
  amazon.aws:
    version: 6.4.0
  ansible.netcommon:
    version: 5.2.0
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.11.0
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.1.0
  awx.awx:
    version: 22.7.0
  azure.azcollection:
    version: 1.17.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.4
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.15
  cisco.meraki:
    version: 2.16.0
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.3.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.1
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.1
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.4.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.5
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.3
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.8.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.26.0
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.2
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.8
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.3.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.2.1
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.14.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.1.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.3
  purestorage.flasharray:
    version: 1.21.0
  purestorage.flashblade:
    version: 1.13.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.14.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.9.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/root/.ansible/collections/ansible_collections:
  ansible.netcommon:
    version: 5.2.0
  ansible.utils:
    version: 2.11.0
  community.crypto:
    version: 2.15.1
  community.general:
    version: 7.4.0
  community.mysql:
    version: 3.7.2
  community.postgresql:
    version: 3.2.0
  hetzner.hcloud:
    version: 2.1.1
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.20.0 using ansible-core:2.15.4 ansible-compat:4.1.10 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
/usr/local/bin/ansible
ansible [core 2.15.4]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.2 using python 3.10
    ansible:2.15.4
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.2 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

## 2023-08-22 images

2023-08-22 images for Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20230822

Debian Trixie with Ansible installed with [version 2.19.1](https://git.coop/webarch/ansible/-/releases/2.19.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.3.0
  ansible.netcommon:
    version: 5.1.2
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.10.3
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.0.1
  awx.awx:
    version: 22.6.0
  azure.azcollection:
    version: 1.16.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.3
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.14
  cisco.meraki:
    version: 2.15.3
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.2.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.0
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.3.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.4
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.2
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.7.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.25.1
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.1
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.5
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.2.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.1.0
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.13.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.0.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.2
  purestorage.flasharray:
    version: 1.20.0
  purestorage.flashblade:
    version: 1.12.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.12.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.8.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.18.0 using ansible-core:2.15.3 ansible-compat:4.1.7 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.4 (main, Jun  7 2023, 10:13:09) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
<unknown>:1: SyntaxWarning: invalid decimal literal
molecule 6.0.1 using python 3.11
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bookworm:20230822

Debian Bookworm with Ansible installed with [version 2.19.1](https://git.coop/webarch/ansible/-/releases/2.19.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.11/site-packages/ansible_collections:
  amazon.aws:
    version: 6.3.0
  ansible.netcommon:
    version: 5.1.2
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.10.3
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.0.1
  awx.awx:
    version: 22.6.0
  azure.azcollection:
    version: 1.16.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.3
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.14
  cisco.meraki:
    version: 2.15.3
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.2.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.0
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.3.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.4
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.2
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.7.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.25.1
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.1
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.5
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.2.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.1.0
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.13.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.0.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.2
  purestorage.flasharray:
    version: 1.20.0
  purestorage.flashblade:
    version: 1.12.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.12.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.8.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.18.0 using ansible-core:2.15.3 ansible-compat:4.1.7 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.11 
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bullseye:20230822

Debian Bullseye with Ansible installed with [version 2.19.1](https://git.coop/webarch/ansible/-/releases/2.19.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.9/site-packages/ansible_collections:
  amazon.aws:
    version: 6.3.0
  ansible.netcommon:
    version: 5.1.2
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.10.3
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.0.1
  awx.awx:
    version: 22.6.0
  azure.azcollection:
    version: 1.16.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.3
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.14
  cisco.meraki:
    version: 2.15.3
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.2.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.0
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.3.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.4
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.2
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.7.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.25.1
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.1
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.5
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.2.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.1.0
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.13.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.0.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.2
  purestorage.flasharray:
    version: 1.20.0
  purestorage.flashblade:
    version: 1.12.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.12.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.8.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.18.0 using ansible-core:2.15.3 ansible-compat:4.1.7 ruamel-yaml:None ruamel-yaml-clib:None
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.9 
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### jammy:20230822

Ubuntu Jammy with Ansible installed with [version 2.19.1](https://git.coop/webarch/ansible/-/releases/2.19.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/opt/venvs/ansible/lib/python3.10/site-packages/ansible_collections:
  amazon.aws:
    version: 6.3.0
  ansible.netcommon:
    version: 5.1.2
  ansible.posix:
    version: 1.5.4
  ansible.utils:
    version: 2.10.3
  ansible.windows:
    version: 1.14.0
  arista.eos:
    version: 6.0.1
  awx.awx:
    version: 22.6.0
  azure.azcollection:
    version: 1.16.0
  check_point.mgmt:
    version: 5.1.1
  chocolatey.chocolatey:
    version: 1.5.1
  cisco.aci:
    version: 2.7.0
  cisco.asa:
    version: 4.0.1
  cisco.dnac:
    version: 6.7.3
  cisco.intersight:
    version: 1.0.27
  cisco.ios:
    version: 4.6.1
  cisco.iosxr:
    version: 5.0.3
  cisco.ise:
    version: 2.5.14
  cisco.meraki:
    version: 2.15.3
  cisco.mso:
    version: 2.5.0
  cisco.nso:
    version: 1.0.3
  cisco.nxos:
    version: 4.4.0
  cisco.ucs:
    version: 1.10.0
  cloud.common:
    version: 2.1.4
  cloudscale_ch.cloud:
    version: 2.3.1
  community.aws:
    version: 6.2.0
  community.azure:
    version: 2.0.0
  community.ciscosmb:
    version: 1.0.6
  community.crypto:
    version: 2.15.0
  community.digitalocean:
    version: 1.24.0
  community.dns:
    version: 2.6.0
  community.docker:
    version: 3.4.8
  community.fortios:
    version: 1.0.0
  community.general:
    version: 7.3.0
  community.google:
    version: 1.0.0
  community.grafana:
    version: 1.5.4
  community.hashi_vault:
    version: 5.0.0
  community.hrobot:
    version: 1.8.1
  community.libvirt:
    version: 1.2.0
  community.mongodb:
    version: 1.6.1
  community.mysql:
    version: 3.7.2
  community.network:
    version: 5.0.0
  community.okd:
    version: 2.3.0
  community.postgresql:
    version: 2.4.3
  community.proxysql:
    version: 1.5.1
  community.rabbitmq:
    version: 1.2.3
  community.routeros:
    version: 2.9.0
  community.sap:
    version: 1.0.0
  community.sap_libs:
    version: 1.4.1
  community.skydive:
    version: 1.0.0
  community.sops:
    version: 1.6.4
  community.vmware:
    version: 3.9.0
  community.windows:
    version: 1.13.0
  community.zabbix:
    version: 2.1.0
  containers.podman:
    version: 1.10.2
  cyberark.conjur:
    version: 1.2.0
  cyberark.pas:
    version: 1.0.19
  dellemc.enterprise_sonic:
    version: 2.2.0
  dellemc.openmanage:
    version: 7.6.1
  dellemc.powerflex:
    version: 1.7.0
  dellemc.unity:
    version: 1.7.1
  f5networks.f5_modules:
    version: 1.25.1
  fortinet.fortimanager:
    version: 2.2.1
  fortinet.fortios:
    version: 2.3.1
  frr.frr:
    version: 2.0.2
  gluster.gluster:
    version: 1.0.2
  google.cloud:
    version: 1.2.0
  grafana.grafana:
    version: 2.1.5
  hetzner.hcloud:
    version: 1.16.0
  hpe.nimble:
    version: 1.1.4
  ibm.qradar:
    version: 2.1.0
  ibm.spectrum_virtualize:
    version: 1.12.0
  infinidat.infinibox:
    version: 1.3.12
  infoblox.nios_modules:
    version: 1.5.0
  inspur.ispim:
    version: 1.3.0
  inspur.sm:
    version: 2.3.0
  junipernetworks.junos:
    version: 5.2.0
  kubernetes.core:
    version: 2.4.0
  lowlydba.sqlserver:
    version: 2.1.0
  microsoft.ad:
    version: 1.3.0
  netapp.aws:
    version: 21.7.0
  netapp.azure:
    version: 21.10.0
  netapp.cloudmanager:
    version: 21.22.0
  netapp.elementsw:
    version: 21.7.0
  netapp.ontap:
    version: 22.7.0
  netapp.storagegrid:
    version: 21.11.1
  netapp.um_info:
    version: 21.8.0
  netapp_eseries.santricity:
    version: 1.4.0
  netbox.netbox:
    version: 3.13.0
  ngine_io.cloudstack:
    version: 2.3.0
  ngine_io.exoscale:
    version: 1.0.0
  ngine_io.vultr:
    version: 1.1.3
  openstack.cloud:
    version: 2.1.0
  openvswitch.openvswitch:
    version: 2.1.1
  ovirt.ovirt:
    version: 3.1.2
  purestorage.flasharray:
    version: 1.20.0
  purestorage.flashblade:
    version: 1.12.1
  purestorage.fusion:
    version: 1.6.0
  sensu.sensu_go:
    version: 1.14.0
  servicenow.servicenow:
    version: 1.0.6
  splunk.es:
    version: 2.1.0
  t_systems_mms.icinga_director:
    version: 1.33.1
  telekom_mms.icinga_director:
    version: 1.34.1
  theforeman.foreman:
    version: 3.12.0
  vmware.vmware_rest:
    version: 2.3.1
  vultr.cloud:
    version: 1.8.0
  vyos.vyos:
    version: 4.1.0
  wti.remote:
    version: 1.0.5
/usr/local/bin/ansible-lint
WARNING: PATH altered to include /opt/venvs/ansible/bin :: This is usually a sign of broken local setup, which can cause unexpected behaviors.
ansible-lint 6.18.0 using ansible-core:2.15.3 ansible-compat:4.1.7 ruamel-yaml:0.17.32 ruamel-yaml-clib:0.2.7
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.10 
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

## 2023-08-16 images

2023-08-16 images for testing Ansible roles, which [include systemd](https://git.coop/webarch/containers/-/issues/5):

### trixie:20230816

Debian Trixie with Ansible installed with [version 2.19.0](https://git.coop/webarch/ansible/-/releases/2.19.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.4 (main, Jun  7 2023, 10:13:09) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.11
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bookworm:20230816

Debian Bookworm with Ansible installed with [version 2.19.0](https://git.coop/webarch/ansible/-/releases/2.19.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.11
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### bullseye:20230816

Debian Bullseye with Ansible installed with [version 2.19.0](https://git.coop/webarch/ansible/-/releases/2.19.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.9
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

### jammy:20230816

Ubuntu Jammy with Ansible installed with [version 2.19.0](https://git.coop/webarch/ansible/-/releases/2.19.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/usr/local/bin/ansible
ansible [core 2.15.3]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/local/bin/ansible
  python version = 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0] (/opt/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/usr/local/bin/yamllint
yamllint 1.32.0
/usr/local/bin/molecule
molecule 6.0.1 using python 3.10
    ansible:2.15.3
    azure:23.5.0 from molecule_plugins
    containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    default:6.0.1 from molecule
    docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.5.0 from molecule_plugins
    gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.5.0 from molecule_plugins
```

## 2023-07-13 images

2023-07-13 images for testing Ansible roles:

### trixie:20230713

```
registry.git.coop/webarch/containers/images/trixie:20230713
```

Debian Trixie with Ansible installed for the `ansible` user with [version 2.14.0](https://git.coop/webarch/ansible/-/releases/2.14.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/home/ansible/.local/bin/ansible
ansible [core 2.15.1]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.11.4 (main, Jun  7 2023, 10:13:09) [GCC 12.2.0] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/home/ansible/.local/bin/yamllint
yamllint 1.32.0
/home/ansible/.local/bin/molecule
molecule 5.1.0 using python 3.11
    ansible:2.15.1
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.1.0 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

### bookworm:20230713

```
registry.git.coop/webarch/containers/images/bookworm:20230713
```

Debian Bookworm with Ansible installed for the `ansible` user with [version 2.14.0](https://git.coop/webarch/ansible/-/releases/2.14.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/home/ansible/.local/bin/ansible
ansible [core 2.15.1]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.11/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/home/ansible/.local/bin/yamllint
yamllint 1.32.0
/home/ansible/.local/bin/molecule
molecule 5.1.0 using python 3.11
    ansible:2.15.1
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.1.0 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

### bullseye:20230713

```
registry.git.coop/webarch/containers/images/bullseye:20230713
```

Debian Bullseye with Ansible installed for the `ansible` user with [version 2.14.0](https://git.coop/webarch/ansible/-/releases/2.14.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/home/ansible/.local/bin/ansible
ansible [core 2.15.1]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/home/ansible/.local/bin/yamllint
yamllint 1.32.0
/home/ansible/.local/bin/molecule
molecule 5.1.0 using python 3.9
    ansible:2.15.1
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.1.0 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

### jammy:20230713

```
registry.git.coop/webarch/containers/images/jammy:20230713
```

Ubuntu Jammy with Ansible installed for the `ansible` user with [version 2.14.0](https://git.coop/webarch/ansible/-/releases/2.14.0) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
/home/ansible/.local/bin/ansible
ansible [core 2.15.1]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.10.6 (main, May 29 2023, 11:10:38) [GCC 11.3.0] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
/home/ansible/.local/bin/yamllint
yamllint 1.32.0
/home/ansible/.local/bin/molecule
molecule 5.1.0 using python 3.10
    ansible:2.15.1
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.1.0 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

## 2013-06-05 images

2013-06-05 images for testing Ansible roles:

### bookworm:20230605

```
registry.git.coop/webarch/containers/images/bookworm:20230605
```

Debian Bookworm with Ansible installed for the `ansible` user with [version 2.13.1](https://git.coop/webarch/ansible/-/releases/2.13.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
ansible [core 2.14.3]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] (/usr/bin/python3)
  jinja version = 3.1.2
  libyaml = True
yamllint 1.32.0
molecule 5.0.1 using python 3.11
    ansible:2.14.3
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.0.1 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

### bullseye:20230605

```
registry.git.coop/webarch/containers/images/bullseye:20230605
```

Debian Bullseye with Ansible installed for the `ansible` user with [version 2.13.1](https://git.coop/webarch/ansible/-/releases/2.13.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
ansible [core 2.14.6]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.9/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
yamllint 1.32.0
molecule 5.0.1 using python 3.9
    ansible:2.14.6
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.0.1 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

### jammy:20230605

```
registry.git.coop/webarch/containers/images/jammy:20230605
```

Ubuntu Jammy with Ansible installed for the `ansible` user with [version 2.13.1](https://git.coop/webarch/ansible/-/releases/2.13.1) of the [Ansible role](https://git.coop/webarch/ansible) which provides:

```
ansible [core 2.14.6]
  config file = None
  configured module search path = ['/home/ansible/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/ansible/.local/pipx/venvs/ansible/lib/python3.10/site-packages/ansible
  ansible collection location = /home/ansible/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/ansible/.local/bin/ansible
  python version = 3.10.6 (main, May 29 2023, 11:10:38) [GCC 11.3.0] (/home/ansible/.local/pipx/venvs/ansible/bin/python)
  jinja version = 3.1.2
  libyaml = True
yamllint 1.32.0
molecule 5.0.1 using python 3.10
    ansible:2.14.6
    azure:23.4.1 from molecule_plugins
    containers:23.4.1 from molecule_plugins requiring collections: ansible.posix>=1.3.0 community.docker>=1.9.1 containers.podman>=1.8.1
    delegated:5.0.1 from molecule
    docker:23.4.1 from molecule_plugins requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0
    ec2:23.4.1 from molecule_plugins
    gce:23.4.1 from molecule_plugins requiring collections: google.cloud>=1.0.2 community.crypto>=1.8.0
    podman:23.4.1 from molecule_plugins requiring collections: containers.podman>=1.7.0 ansible.posix>=1.3.0
    vagrant:23.4.1 from molecule_plugins
```

## Containers no longer being rebuilt

### Ansible

This image is designed to be used by GitLab CI for running Ansible playbooks.

The `registry.git.coop/webarch/containers/images/ansible:0.24.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-05-15 plus Ansible configured using [version 2.12.2](https://git.coop/webarch/ansible/-/releases/2.12.2) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.2` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.23.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-04-21 plus Ansible configured using [version 2.1.0](https://git.coop/webarch/ansible/-/releases/2.1.0) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.2` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.22.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-03-20 plus Ansible configured using [version 2.0.2](https://git.coop/webarch/ansible/-/releases/2.0.2) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.21.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-02-18 plus Ansible configured using [version 1.9.0](https://git.coop/webarch/ansible/-/releases/1.9.0) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.20.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-02-15 plus Ansible configured using [version 1.8.5](https://git.coop/webarch/ansible/-/releases/1.8.5) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.19.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-02-05 plus Ansible configured using [version 1.8.3](https://git.coop/webarch/ansible/-/releases/1.8.3) of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.18.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-01-19 plus Ansible configured using version 1.5.6 of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.17.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2023-01-13 plus Ansible configured using version 1.5.1 of the [Ansible role](https://git.coop/webarch/ansible), [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.16.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-12-22 plus [Ansible 7.1.0](https://packages.debian.org/bookworm/ansible) and [Ansible 2.14.0](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.10.0](https://github.com/ansible/ansible-lint/releases/tag/v6.10.0) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-12-16, which was `4.0.4`, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.15.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-12-16 plus [Ansible 7.1.0](https://packages.debian.org/bookworm/ansible) and [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.10.0](https://github.com/ansible/ansible-lint/releases/tag/v6.10.0) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-12-16, which was `4.0.4`, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.14.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-12-06 plus [Ansible 7.0.0](https://packages.debian.org/bookworm/ansible) and [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.9.1](https://github.com/ansible/ansible-lint/releases/tag/v6.9.1) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-12-06, which was `4.0.4`, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) version `2.2.0` for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.13.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-12-05 plus [Ansible 7.0.0](https://packages.debian.org/bookworm/ansible) and [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.9.1](https://github.com/ansible/ansible-lint/releases/tag/v6.9.1) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-12-05, which was 4.0.4, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates plus the [webarch-ssh](https://git.coop/webarch/webarch-ssh) repo which includes SSH fingerprints for Webarchitects servers.

The `registry.git.coop/webarch/containers/images/ansible:0.12.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-10-13 plus [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.8.1](https://github.com/ansible/ansible-lint/releases/tag/v6.8.1) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-10-13, which was 4.0.1, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates.

The `registry.git.coop/webarch/containers/images/ansible:0.11.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-09-21 plus [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://ansible-lint.readthedocs.io/installing/) version [6.6.1](https://github.com/ansible/ansible-lint/releases/tag/v6.6.1) installed via `pip3`, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-09-20, which was 4.0.1, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates.

The `registry.git.coop/webarch/containers/images/ansible:0.10.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-09-18 plus [Ansible 2.13.4](https://packages.debian.org/bookworm/ansible-core) from Debian, [ansible-lint](https://packages.debian.org/bookworm/ansible-lint) from Debian, the latest versions of Ansible galaxy collections, `ansible.posix`, `community.docker`, `community.crypto`, `community.general`, `community.mysql`, `community.postgresql` and `community.rabbitmq` version `1.2.2`, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-09-18, which was 4.0.1, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates.

The `registry.git.coop/webarch/containers/images/ansible:0.9.0` image is [Debian](https://hub.docker.com/_/debian) Bookworm from 2022-08-26 plus [Ansible 2.12.4](https://packages.debian.org/bookworm/ansible) and [ansible-lint](https://packages.debian.org/bookworm/ansible-lint) from Debian, the latest version of [molcule](https://pypi.org/project/molecule/) as of 2022-08-26, which was 4.0.1, installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates.

The `registry.git.coop/webarch/containers/images/ansible:0.8.0` image is [Debian](https://hub.docker.com/_/debian) Bullseye 11.1 plus [Ansible 2.10.x](https://packages.debian.org/bullseye/ansible) from Debian, `molcule` and `ansible-lint` version `3.5.2` installed using `pip3` plus `ansiblej2lint.py` from [jinja2-lint](https://github.com/webarch-coop/jinja2-lint) for checking Jinja2 templates.

The `registry.git.coop/webarch/containers/images/ansible:0.7.0` image is [Debian](https://hub.docker.com/_/debian) Bullseye 11.1 plus [Ansible 2.10.x](https://packages.debian.org/bullseye/ansible) from Debian, `molcule` and `ansible-lint` version `3.5.2` installed using `pip3`.

The `registry.git.coop/webarch/containers/images/ansible:0.6.0` image is [Debian](https://hub.docker.com/_/debian) Buster 10.9 plus [Ansible 2.9.16](https://packages.debian.org/buster-backports/ansible) from Debian Backports, `molcule` and `ansible-lint` version `3.3.0` installed using `pip3`.

The `registry.git.coop/webarch/containers/images/ansible:0.5.0` image is [Debian](https://hub.docker.com/_/debian) Buster 10.8 plus [Ansible 2.9.17](https://github.com/ansible/ansible/blob/stable-2.9/changelogs/CHANGELOG-v2.9.rst#release-summary), `molcule` and `ansible-lint` installed using `pip3`.

The `registry.git.coop/webarch/containers/images/ansible:0.4` image is [Debian](https://hub.docker.com/_/debian) Buster 10.4 plus [Ansible 2.9.9](https://github.com/ansible/ansible/blob/stable-2.9/changelogs/CHANGELOG-v2.9.rst#release-summary), `molcule` and `ansible-lint` installed using `pip3`.

The `registry.git.coop/webarch/containers/images/ansible:0.3` image is [Debian](https://hub.docker.com/_/debian) Buster 10.2 plus the latest version of `ansible`, `molcule` and `ansible-lint` installed using `pip3`.

The `registry.git.coop/webarch/containers/images/ansible:0.2` image is [Debian](https://hub.docker.com/_/debian) Buster 10.2 plus `ansible` and `ansible-lint` with `molcule` installed using `pip` (the version of Ansible installed by pip in `/usr/local/bin` has been deleted to ensure that the Debian version of Ansible is used).

The `registry.git.coop/webarch/containers/images/ansible:0.1` image is [Debian](https://hub.docker.com/_/debian) Stretch plus [Ansible from Stretch Backports](https://packages.debian.org/stretch-backports/ansible) and `ansible-lint` and `molcule` installed using `pip`.

### DNS

This image is designed to be used by GitLab CI for running DNS checks and updates.

The `registry.git.coop/webarch/containers/images/dns:0.5.0` image is [Debian](https://hub.docker.com/_/debian) Bullseye 11.4 plus various DNS related utilities (`bind9utils`, `dnsutils`, `wget`, `whois` and `git`).

The `registry.git.coop/webarch/containers/images/dns:0.4.0` image is [Debian](https://hub.docker.com/_/debian) Buster 10.9 plus various DNS related utilities (`bind9utils`, `dnsutils`, `wget`, `whois` and `git`).

The `registry.git.coop/webarch/containers/images/dns:0.3.0` image is [Debian](https://hub.docker.com/_/debian) Buster 10.8 plus various DNS related utilities (`bind9utils`, `dnsutils`, `wget`, `whois` and `git`).

The `registry.git.coop/webarch/containers/images/dns:0.2` image is [Debian](https://hub.docker.com/_/debian) Buster 10.4 plus various DNS related utilities (`bind9utils`, `dnsutils`, `wget`, `whois` and `git`).

The `registry.git.coop/webarch/containers/images/dns:0.1` image is [Debian](https://hub.docker.com/_/debian) Stretch 9.13 plus various DNS related utilities (`bind9utils`, `dnsutils`, `wget`, `whois` and `git`).

### Jekyll

This image is designed to be used to build and deploy [Jekyll](https://jekyllrb.com/).

The `registry.git.coop/webarch/containers/images/jekyll:0.4.0` image is [Debian](https://hub.docker.com/_/debian) Bullseye 11.4 plus [Jekyll v4.2.1](https://github.com/jekyll/jekyll/releases/tag/v4.2.1) and the [requirements](https://jekyllrb.com/docs/installation/#requirements).

The `registry.git.coop/webarch/containers/images/jekyll:0.3.0` image is [Debian](https://hub.docker.com/_/debian) Buster 10.8 plus [Jekyll v4.2.0](https://github.com/jekyll/jekyll/releases/tag/v4.2.0) and the [requirements](https://jekyllrb.com/docs/installation/#requirements).

The `registry.git.coop/webarch/containers/images/jekyll:0.2` image is [Debian](https://hub.docker.com/_/debian) Buster 10.4 plus [Jekyll v4.1.0](https://github.com/jekyll/jekyll/releases/tag/v4.1.0) and the [requirements](https://jekyllrb.com/docs/installation/#requirements).

The `registry.git.coop/webarch/containers/images/jekyll:0.1` image is [Debian](https://hub.docker.com/_/debian) Buster 10.3 plus the [requirements](https://jekyllrb.com/docs/installation/#requirements).

### CI

This image is designed to be used by GitLab CI runner for Molecule testing roles using Docker-in-Docker.

## Versioning

We aim to use [semver](https://semver.org/) for the container image tags.
